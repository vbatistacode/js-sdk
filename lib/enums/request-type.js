"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.XanoRequestType = void 0;
var XanoRequestType;
(function (XanoRequestType) {
    XanoRequestType["DELETE"] = "DELETE";
    XanoRequestType["GET"] = "GET";
    XanoRequestType["HEAD"] = "HEAD";
    XanoRequestType["PATCH"] = "PATCH";
    XanoRequestType["POST"] = "POST";
    XanoRequestType["PUT"] = "PUT";
})(XanoRequestType = exports.XanoRequestType || (exports.XanoRequestType = {}));
//# sourceMappingURL=request-type.js.map