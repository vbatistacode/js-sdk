/// <reference types="node" />
export declare class XanoFile {
    private name;
    private buffer;
    constructor(name: string, buffer: Buffer);
    getBuffer(): Buffer;
    getName(): string;
}
//# sourceMappingURL=file.d.ts.map